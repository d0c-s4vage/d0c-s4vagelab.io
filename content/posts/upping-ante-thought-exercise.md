---
title: "Upping Ante Thought Exercise"
date: '2010-08-12T22:25:00.000-07:00'
categories:
  - browser
  - thoughts
---

**Moved from [d0cs4vage.blogspot.com](http://d0cs4vage.blogspot.com/2010/08/upping-ante-thought-exercise.html) to here**

**WARNING:** _This post contains a trip into the wondrous what-if-land to
explore a different means of increasing security for the end-user._

I was thinking the other day about how the landing pages for browsers (and
other products) usually tout security as one of its main attributes. Below
are a few examples:

---

[Firefox](http://www.mozilla.com/en-US/firefox/firefox.html#feature-security):

What makes Firefox the best?

Keeping you safe while you surf is our top priority, which is why Firefox
includes advanced anti-phishing and anti-malware technologies plus features
like private browsing and “forget this site” to ensure your privacy.

Plus, our open source security process means we have experts around the
globe working around the clock to keep you (and your personal information)
safe.

[Google Chrome](http://www.google.com/chrome/intl/en/more/security.html):

Google Chrome includes features to help protect you and
your computer from malicious websites as you browse the web. Chrome uses
technologies such as Safe Browsing, sandboxing, and auto-updates to help
protect you against phishing and malware attacks.

[Internet Explorer](http://www.microsoft.com/windows/internet-explorer/default.aspx):

Browser Security

Other browsers leave you more exposed to phishing, malware and other online
threats. Or they require you to download and configure third-party add-ons just
to get the security Internet Explorer 8 comes with right out of the box

[Opera](http://www.opera.com/browser/):

Safe and secure

Reduce your exposure to threats on the Web. Industry-leading security and
features such as Web Threat Protection and a security bar, ensure your
safety.

---

To me each of them is being _very_ conservative when it comes to describing
how secure their product is. Each of those companies has extremely intelligent
people from the security field working for them, about whom each company
could boast. They could also talk about how few bugs have been found in their
product, or their awesome fuzzing-farm that should dramatically reduce the
number of 0days available for researchers to find. Or, if they wanted to
get really crazy, they could even publicly bash their competitors, saying
things like "Browser A sucks! BB security vulnerabilities were found in it
in the past CC months! Only DD vulns were found in ours!"

What if companies actually _did_ focus on publicly bashing their
competitors? The end goal for a company making a product is to increase
their user-base and hence their profits, right? If they could make their
competitor's product look like swiss cheese because of the number of security
holes in it, they might very effectively keep existing users from switching
to a competitor. They could also try to "prove" how little their product
resembles swiss cheese, drawing in more users.

So far, this all doesn't seem too far-fetched. Often, I think of competing
companies as being relatively nice towards each other when they find security
vulnerabilities in the competitor's product. The vuln might get reported
responsibly and then isn't really mentioned once it's fixed. But this is
all different in what-if land. Imagine a world where they weren't publicly
"nice" to each other. In this world, each company's security team would
have two main job functions: fix our product, and find security holes in
the competition so we can publicly humiliate them!

If this were the case, everything would be nuts and highly
entertaining! Security marketing for a product would be more along the
lines of "Don't use product EE! Our security team alone has recently found
FF vulnerabilities in their product, we have only had [some # smaller than
FF]!" or "Our security team finds more bugs in our competitor's products
than they can manage to find and fix on their own! Imagine how secure our
product is with such a team securing it!" or even more crass "Product GG
has been found to be vulnerable to HH! We're not, use us! If you use them,
we'll pwn you with the 0days we've been saving!" Once a company released a
statement publicly bashing a competitor, the competitor would be forced to
defend themselves in some form or another in order to keep their existing
user-base. They might counter-attack, releasing advisories and embarrassing
the attacker, or they might show how security feature MM effectively thwarts
those attack vectors. If a company has no comeback, their user-base will
dwindle until they can save face before their users.

Mainstream media would, of course, have a heyday with any and all public
bashing. News stories with headlines such as "Company II publicly humiliates
Company JJ! JJ declares war on II!" would abound, increasing the drama and
upping the ante and humiliation.

But who would all of this actually benefit? Well, that should be fairly
obvious, actually: the end-user! (Hey, isn't that what all this security stuff
is about anyways?!) With all of the public bashing, humiliating, security
dream-team putting-togethering going on, the actual products themselves
will become more secure, either directly or indirectly. The rate at which
bugs are found and fixed in a product would be amplified tremendously. New
security features would constantly be thought up to stave off attacks and
to undermine the competitor's claims. New policies would be put into place
in companies to _quickly_ provide fixes for the end-user. Companies would
scour the globe looking for the best in the security field to help them defend
their product and attack the competitors. In the end, by the time the product
reaches the end-user, it would be more secure. The end-user would also have
a good time sitting back and enjoying the drama! Heck, companies could even
sell tickets! We could make a sporting event out of it!

However, this wouldn't only help the end-user: it would help security
researchers tremendously. As a result of the need for sufficiently
good bash-ammunition and defensive armor a company will need against its
competitors in order to stay alive, large companies that currently do not pay
for vulnerabilities would be forced to pay. Not only would they be forced
to pay, they would have to compete for security researcher's attention by
constantly increasing the incentive. It's a win-win situation!

Of course there are some pitfalls in this thinking that I haven't mentioned,
such as a large company picking on a small start-up who doesn't yet have
the means to compete or defend themselves, or companies targeting specific
individuals in the competition instead of the company they work for. In such
a case, referees would have to be appointed who could rebuke the offending,
rule-breaking company and publicly shame them for being a bully. The larger
company then might have to help the smaller company out with it's security
team and policies until it's able to defend itself. If an individual was
personally harassed, the company should have to publicly apologize and send
him a fruit-basket and a "Get-Well" card.

Crazy idea, huh? There are several other points I thought about bringing up,
but I think I demonstrated what I wanted to. Would this really work? Probably
not. It'd be crazy if it did though. Am I serious about this post? Partly
(remember, this was only a thought-exercise!)

Even though this is an extreme example, the point I really wanted to
make with this post is how different things might be for the end-user if
companies personally had more at stake for creating (and selling) insecure
products. My conclusion is that things could be very different from how
they are today. Companies would have bigger/better security teams, different
bug-fixing policies requiring fast turn around times for patch development,
and the incentive for reporting vulnerabilities directly to the vendor
would be at least better than the possibility of having your name in the
acknowledgment section of an advisory. I think it might be a good thing.

PS - _On a side note, in case you needed more food for thought: Does the
problem of finding and fixing every security vulnerability in a product
belong in P, NP, or is it NP complete?_
