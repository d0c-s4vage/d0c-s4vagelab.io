---
title: Windbg Tricks - Module Relocation
date: '2013-04-13T13:55:00.000-07:00'
author: d0c.s4vage
categories:
  - windbg trick
  - aslr
---

**Moved from [d0cs4vage.blogspot.com](http://d0cs4vage.blogspot.com/2013/04/windbg-tricks-module-relocation.html) to here**

When ASLR is not supported, pseudo ASLR is often used to introduce a degree
of entropy in where the module is loaded into memory.

The basic idea behind pseudo ASLR is to pre-allocate memory at the location
of a module's preferred base address. This forces the module to be loaded
at a non-predetermined address. See [this](http://lmgtfy.com/?q=pseudo+aslr)
for more details.

I stumbled across the windbg command `!imgreloc`
the other day. It can be used to show all modules that have been relocated,
and what their original preferred base address is.

Below is the output when run while attached to firefox.exe (see [this ticket
about dll blocking](https://bugzilla.mozilla.org/show_bug.cgi?id=642365) and
[this firefox ticket](https://bugzilla.mozilla.org/show_bug.cgi?id=677797)
for a specific history of pseudo ASLR in firefox):

```
0:017> !imgreloc
00280000 sqlite3 - RELOCATED from 10000000
00300000 js3250 - RELOCATED from 10000000
00400000 firefox - at preferred address
004e0000 nspr4 - RELOCATED from 10000000
00510000 smime3 - RELOCATED from 10000000
00530000 nss3 - RELOCATED from 10000000
005d0000 nssutil3 - RELOCATED from 10000000
005f0000 plc4 - RELOCATED from 10000000
00600000 plds4 - RELOCATED from 10000000
00610000 ssl3 - RELOCATED from 10000000
00640000 xpcom - RELOCATED from 10000000
01220000 browserdirprovider - RELOCATED from 10000000
01540000 brwsrcmp - RELOCATED from 10000000
01de0000 nssdbm3 - RELOCATED from 10000000
02000000 xpsp2res - RELOCATED from 00010000
036a0000 softokn3 - RELOCATED from 10000000
03980000 freebl3 - RELOCATED from 10000000
039d0000 nssckbi - RELOCATED from 10000000
10000000 xul - at preferred address
59a60000 dbghelp - at preferred address
5ad70000 uxtheme - at preferred address
```

```
0:017> .shell -ci "!imgreloc" findstr RELOCATED
00280000 sqlite3 - RELOCATED from 10000000
00300000 js3250 - RELOCATED from 10000000
004e0000 nspr4 - RELOCATED from 10000000
00510000 smime3 - RELOCATED from 10000000
00530000 nss3 - RELOCATED from 10000000
005d0000 nssutil3 - RELOCATED from 10000000
005f0000 plc4 - RELOCATED from 10000000
00600000 plds4 - RELOCATED from 10000000
00610000 ssl3 - RELOCATED from 10000000
00640000 xpcom - RELOCATED from 10000000
01220000 browserdirprovider - RELOCATED from 10000000
01540000 brwsrcmp - RELOCATED from 10000000
01de0000 nssdbm3 - RELOCATED from 10000000
02000000 xpsp2res - RELOCATED from 00010000
036a0000 softokn3 - RELOCATED from 10000000
03980000 freebl3 - RELOCATED from 10000000
039d0000 nssckbi - RELOCATED from 10000000
```

Searching for `preferred` instead of `RELOCATED` will yield a list of modules
that should remain at their preferred address (and thus be usable for ROP
or other such techniques).
