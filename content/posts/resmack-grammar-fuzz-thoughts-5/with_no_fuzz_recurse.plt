##

set terminal qt noenhanced
set key off
#set key at screen 1, graph 1
#set key autotitle columnhead
#stats 'with_no_depth_fuzz_processed.log' using 0 nooutput
set xrange [0:100]
set yrange [0:10000000]
set logscale y

set boxwidth 0.95
set style fill solid
#set palette rgb 3,11,6
#set palette negative
#plot 'with_no_depth_fuzz_processed.log' using ($0):($1):($1) with boxes palette
plot 'with_no_depth_fuzz_processed.log' with boxes lc 1
#plot 'with_depth_fuzz_processed.log' with boxes lc 2
#plot 'with_depth_fuzz_processed.log' using ($0):($1):($1) with boxes palette
