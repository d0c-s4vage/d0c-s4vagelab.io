+++ 
draft = true
date = 2020-06-26T21:10:26-07:00
title = "Resmack: Fuzzing Thoughts - Part 2"
description = "ptrace feedback snapshot fuzzing"
slug = ""
tags = ["ptrace", "feedback", "snapshot fuzzing", "fuzzing"]
categories = ["fuzzing"]
externalLink = ""
series = []
+++

I took a small break from working on [resmack-rust](https://gitlab.com/d0c-s4vage/resmack-rust)
(renamed to resmack-rust to free up the name resmack)
to explore writing a snapshot-based fuzzer.

Often my "fuzzers" focus very heavily on the data generation aspect
(see [gramfuzz](https://github.com/d0c-s4vage/gramfuzz),
[pfp](https://github.com/d0c-s4vage/pfp)), with the debugging/fuzz-harness/etc being
almost an afterthought.

I won't be doing the same thing with resmack. Although I am starting with the
data generation, my plan is to turn it into a full fuzzer, capable of
compiling/launching/instrumenting target programs and performing genetic
mutations (mutation/crossover) on the raw data and the grammar states
that are saved into the corpus.

With that being said, this post is specifically about the "full" fuzzer
experiment and a few lessons I learned.

## Fuzzing Ptrace Test

[fuzzing-ptrace-test](https://gitlab.com/d0c-s4vage/fuzzing-ptrace-test) is a feedback-driven, snapshot-based fuzzer that uses:

* ptrace
* process snapshotting/restoring
    * using `/proc/pid/map`
    * using `process_vm_writev`
* dynamic memory breakpoints specified by the target process
* attempt at using performance counters as a feedback mechanism

In addition to using existing fuzzers for reference [^a] [^b] [^c] [^d], fortuitous
timing made it so that I had a fresh C reference of a nearly identical project to
compare my Rust code to - [@h0mbre_](https://twitter.com/h0mbre_)'s
[Fuzzing Like A Caveman 4: Snapshot/Code Coverage Fuzzer](https://h0mbre.github.io/Fuzzing-Like-A-Caveman-4/)

[^a]: libfuzzer: https://llvm.org/docs/LibFuzzer.html
[^b]: honggfuzz: https://github.com/google/honggfuzz
[^c]: AFL: https://github.com/google/AFL
[^d]: AFL++ https://github.com/AFLplusplus/AFLplusplus

### Ptrace

`fuzzing-ptrace-test` uses ptrace to:

* Debug the target process
* Catch signals
* Capture register values

These are used along with the inline assembly in the
contrived target program to signal:

* the pointer to the input data
* the original [max] size of the input data
* the end of meaningful execution

The target process is launched and traced by spawning the new process,
setting [`PTRACE_TRACEME`](http://manpages.ubuntu.com/manpages/precise/man2/ptrace.2.html),
and using [`waitpid()`](http://manpages.ubuntu.com/manpages/xenial/en/man2/waitpid.2.html)
to wait for events from the tracee:

```rust
let child = unsafe {
    println!("Spawning {}", target_bin);
    Command::new(target_bin)
        .arg("AAAA")
        .pre_exec(|| {
            ptrace::traceme().expect("Could not trace process");
            Ok(())
        })
        .spawn()
        .expect("Could not spawn")
};
let child_pid = Pid::from_raw(child.id() as i32);

...

loop {
    match waitpid(child_pid, None) {
        Ok(WaitStatus::Exited(_, status)) => ...
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => ...
        Ok(WaitStatus::Stopped(pid, signal)) => ...
        Ok(WaitStatus::Continued(_)) => ...
        Ok(WaitStatus::PtraceEvent(pid, signal, v)) => ...
        Ok(WaitStatus::Signaled(pid, signal, val)) => ...
        Ok(WaitStatus::StillAlive) => ...
        Ok(WaitStatus::PtraceSyscall(pid)) => ...
        Err(v) => ...
    }
}
```

#### Memory Breakpoints

Memory breakpoints are [set and cleared](https://gitlab.com/d0c-s4vage/fuzzing-ptrace-test/-/blob/91c606cb4b4f6f04601e51a2da02116dedb35b4a/src/main.rs#L99)
by setting values on the `dr0` debug register and the `dr7` debug control
register via `PTRACE_POKEUSER` ptrace requests:

```rust
fn set_watchpoint(child_pid: Pid, mut addr: u64, size: WatchpointSize, reset: bool) {
    unsafe {
        let mut debug_reg: Dr7 = Dr7 { data: [0, 0, 0, 0] };

        if !reset {
            // enable for local tasks only
            debug_reg.set_l0(1);
            debug_reg.set_dr0_break(WatchpointBreakType::OnRw as u8);
            debug_reg.set_dr0_len(size as u8);
            debug_reg.set_reserved_10(1);
            debug_reg.set_le(1);
            debug_reg.set_ge(1);
            debug_reg.set_gd(1);
        } else {
            addr = 0;
        }

        let debug_reg_off = offset_of!(libc::user, u_debugreg);

        // https://docs.rs/libc/0.2.62/libc/struct.user.html#structfield.u_debugreg
        // https://docs.rs/memoffset/0.2.1/memoffset/macro.offset_of.html
        // https://stackoverflow.com/questions/8941711/is-it-possible-to-set-a-gdb-watchpoint-programmatically
        let res = libc::ptrace(
            ptrace::Request::PTRACE_POKEUSER as ptrace::RequestType,
            child_pid,
            debug_reg_off,
            addr,
        );
        if res != 0 {
            let error = errno::from_i32(errno::errno());
            panic!("Error: {}", error);
        }

        let debug_val: u32 = (u32::from(debug_reg.data[3]) << 24)
            | u32::from(debug_reg.data[2]) << 16
            | u32::from(debug_reg.data[1]) << 8
            | u32::from(debug_reg.data[0]);
        let res = libc::ptrace(
            ptrace::Request::PTRACE_POKEUSER as ptrace::RequestType,
            child_pid,
            debug_reg_off + (8 * 7),
            debug_val,
        );
        if res != 0 {
            let error = errno::from_i32(errno::errno());
            panic!("Error: {}", error);
        }
        println!("second poke: {}", res);
    };
}
```

Up to four memory breakpoints can be set using this method with registers
[`dr0` through `dr3`](https://en.wikipedia.org/wiki/X86_debug_register#DR0_to_DR3).

Although setting a memory breakpoint on one, two, four, and eight bytes is
technically allowed, I could only get breakpoints of size one and two to work.
Sizes four and eight always errored. I did most of this development on my favorite
old laptop (Lenovo T420) - it may be something to do with the old hardware.

##### Notes on `dr7` Values

Reading the [Intel manual](https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-3b-part-2-manual.pdf)
on the debug register is highly recommended. A few non-intuitive points that I
used from the manual:

**the `le` and `ge` bits should be set to `1` for exact breakpoint**

If these are set, the processor will break on the exact instruction that caused
the breakpoint condition.

> LE and GE (local and global exact breakpoint enable) flags (bits 8, 9) —
>
> This feature is not supported in the P6 family processors, later IA-32 processors,
> and Intel 64 processors. When set, these flags cause the processor to detect the
> exact instruction that caused a data breakpoint condition. For backward and
> forward compatibility with other Intel processors, we recommend that the LE and
> GE flags be set to 1 if exact breakpoints are required.

**the `gd` bit should be set to `1` to break *before* a `mov` occurs**

> GD (general detect enable) flag (bit 13) —
>
> Enables (when set) debugregister protection, which causes a debug exception to be generated prior to any
> MOV instruction that accesses a debug register. When such a condition is
> detected, the BD flag in debug status register DR6 is set prior to generating the
> exception. This condition is provided to support in-circuit emulators.
> When the emulator needs to access the debug registers, emulator software can
> set the GD flag to prevent interference from the program currently executing on
> the processor.
> The processor clears the GD flag upon entering to the debug exception handler,
> to allow the handler access to the debug registers.


**the 10th bit should always be set to `1`**

Notice the 10th (reserved) bit is hard-coded to `1`:

![Intel dr7 layout](intel_dr7_layout.png)



### Contrived Target

In [target.c](https://gitlab.com/d0c-s4vage/fuzzing-ptrace-test/-/blob/main/target.c),
two blocks of inline assembly exist.

The [first inline assembly block](https://gitlab.com/d0c-s4vage/fuzzing-ptrace-test/-/blob/91c606cb4b4f6f04601e51a2da02116dedb35b4a/target.c#L54-62)
sets the `rax` register to `argv[1]`, and the
`rbx` register to `strlen(argv[1])`, and then triggers a breakpoint (`int3`):

```c
asm("mov $0xf00dfeed, %%rcx;"
    "mov %0, %%rax;"
    "mov %1, %%rbx;"
    "int3;"
    "xor %%rcx, %%rcx;"
     :
     : "r" (argv[1]), "r" (strlen(argv[1]))
     : "rcx", "rbx", "rax"
);
```

When this breakpoint is handled by the fuzzer, the pointer (`rax`) and size
(`rbx`) of the input data are extracted from the target. A memory breakpoint is
then set on the input pointer. `0xf00dfeed` in `rcx` is used to indicate that the
breakpoint is the one that contains the pointer and size in `rax` and `rbx`:

```rust
if regs.rcx == 0xf00dfeed {
    println!("Received tagged memory address");
    println!("| bp addr: {:x}", regs.rax);
    println!("| max_mem_len: {}", regs.rbx);
    println!("| rip: {:x}", regs.rip);
    _overwrite_data_addr = Some(regs.rax);
    _overwrite_data_max_len = Some(regs.rbx as usize);
    set_watchpoint(child_pid, regs.rax, WatchpointSize::One, false);
}
```

Once the memory breakpoint is hit, a snapshot is taken of all writable pages
in the target, as well as the current register state. See the [snapshotting
section](#process-snapshotting).

At this point, fuzzing iterations begin (see the [core fuzzing section](#core-fuzzing)),
with registers and modified pages being restored, and with the input data being
overwritten by new, random data.

The target process then continues until the
[final, manual breakpoint](https://gitlab.com/d0c-s4vage/fuzzing-ptrace-test/-/blob/91c606cb4b4f6f04601e51a2da02116dedb35b4a/target.c#L67-73)
is hit:

```c
asm("mov $0xfeedf00d, %%rcx;"
    "int3;"
    "xor %%rcx, %%rcx;"
     :
     :
     : "%rcx"
);
```

at which point the process snapshot is restored and the next iteration continues.

### Slow Target "Prelude"

[The target](https://gitlab.com/d0c-s4vage/fuzzing-ptrace-test/-/blob/91c606cb4b4f6f04601e51a2da02116dedb35b4a/target.c#L36-44)
was intentionally made to have a slow "prelude" before getting to
the actual code that should be fuzzed:

```c
char buf1[0x10000];
char buf2[0x10000];
PRINT("Long prelude");
for (int i = 0; i < 0x10000; i++) {
    memcpy(buf1, buf2, sizeof(buf1));
}
PRINT("Done with long prelude");
```

Using the memory breakpoint to wait until the target data is actually used
allows us to skip over this expensive section of code before taking the
snapshot. Not skipping over this results in ~2300x fewer iterations per second.

### Process Snapshotting

Process snapshot taking and restoring makes use of a few features of
[procfs](https://man7.org/linux/man-pages//man5/procfs.5.html) to capture and
restore process state:

* `/proc/pid/map`
* [`process_vm_writev`](http://manpages.ubuntu.com/manpages/cosmic/man2/process_vm_readv.2.html)

That's it! At one point fuzzing-ptrace-test used `/proc/pid/map` with only
`/proc/pid/mem`, and then switched to `/proc/pid/clear_refs` and `/proc/pid/pagemap`.
The [Notes on Snapshotting and Procfs](#notes-on-procfs) section goes into detail, but the
short of it is that it was faster to keep it simple and not use `mem`, `clear_refs`,
or `pagemap`.

Snapshotting in this project is done by:

* Identifying all writeable regions in target process' memory
* Saving all:
    * Writeable regions
    * Register Values
* \<doing work\>
* Restoring [changed?] target process state and registers

#### Determining Writeable Regions with `/proc/pid/maps`

This procfs file shows information about the mapped regions of the processes memory.
E.g.:

```
$> cat /proc/self/maps
56548d283000-56548d285000 r--p 00000000 fd:01 36700321                   /usr/bin/cat
56548d285000-56548d28a000 r-xp 00002000 fd:01 36700321                   /usr/bin/cat
56548d28a000-56548d28d000 r--p 00007000 fd:01 36700321                   /usr/bin/cat
56548d28d000-56548d28e000 r--p 00009000 fd:01 36700321                   /usr/bin/cat
56548d28e000-56548d28f000 rw-p 0000a000 fd:01 36700321                   /usr/bin/cat
56548ebaa000-56548ebcb000 rw-p 00000000 00:00 0                          [heap]
7fa2d267c000-7fa2d269e000 rw-p 00000000 00:00 0 
7fa2d269e000-7fa2d2c0e000 r--p 00000000 fd:01 36704859                   /usr/lib/locale/locale-archive
7fa2d2c0e000-7fa2d2c33000 r--p 00000000 fd:01 36703856                   /usr/lib/x86_64-linux-gnu/libc-2.30.so
7fa2d2c33000-7fa2d2dab000 r-xp 00025000 fd:01 36703856                   /usr/lib/x86_64-linux-gnu/libc-2.30.so
7fa2d2dab000-7fa2d2df5000 r--p 0019d000 fd:01 36703856                   /usr/lib/x86_64-linux-gnu/libc-2.30.so
7fa2d2df5000-7fa2d2df8000 r--p 001e6000 fd:01 36703856                   /usr/lib/x86_64-linux-gnu/libc-2.30.so
7fa2d2df8000-7fa2d2dfb000 rw-p 001e9000 fd:01 36703856                   /usr/lib/x86_64-linux-gnu/libc-2.30.so
7fa2d2dfb000-7fa2d2e01000 rw-p 00000000 00:00 0 
7fa2d2e19000-7fa2d2e1a000 r--p 00000000 fd:01 36703852                   /usr/lib/x86_64-linux-gnu/ld-2.30.so
7fa2d2e1a000-7fa2d2e3c000 r-xp 00001000 fd:01 36703852                   /usr/lib/x86_64-linux-gnu/ld-2.30.so
7fa2d2e3c000-7fa2d2e44000 r--p 00023000 fd:01 36703852                   /usr/lib/x86_64-linux-gnu/ld-2.30.so
7fa2d2e45000-7fa2d2e46000 r--p 0002b000 fd:01 36703852                   /usr/lib/x86_64-linux-gnu/ld-2.30.so
7fa2d2e46000-7fa2d2e47000 rw-p 0002c000 fd:01 36703852                   /usr/lib/x86_64-linux-gnu/ld-2.30.so
7fa2d2e47000-7fa2d2e48000 rw-p 00000000 00:00 0 
7ffea9bee000-7ffea9c10000 rw-p 00000000 00:00 0                          [stack]
7ffea9cc6000-7ffea9cc9000 r--p 00000000 00:00 0                          [vvar]
7ffea9cc9000-7ffea9cca000 r-xp 00000000 00:00 0                          [vdso]
ffffffffff600000-ffffffffff601000 --xp 00000000 00:00 0                  [vsyscall]
```

Similar to normal file system permissions, the writable regions of the target
process' memory are indicated with `-w--`.

Copying the data at specific ranges can be done by reading seeking to the
desired address and reading directly from `/proc/pid/mem` or by using
[`process_vm_readv`](http://manpages.ubuntu.com/manpages/cosmic/man2/process_vm_readv.2.html)

#### Finding Dirty Pages With `/proc/pid/clear_refs` and `/proc/pid/pagemap`

Thanks to the [Checkpoint/Restore In Userspace (CRIU) Project](https://criu.org),
a way exists to find which pages of a process been modified since a certain
time. This may appear to be an ideal way to efficiently update only the pages
of the target process that were modified instead of resetting all writeable
regions in the process' memory. However, this did not turn out to be the case
for my small contrived target.

Writing `4` to the file `/proc/pid/clear_refs` will clear all `soft-dirty` bits
on the target process. From the
[procfs man page](http://manpages.ubuntu.com/manpages/xenial/man5/proc.5.html):

> 4 (since Linux 3.11)
> 
> Clear the soft-dirty bit for all the  pages  associated  with  the  process.
> This  is  used  (in conjunction with /proc/[pid]/pagemap) by the check-point
> restore system to discover which pages of a process have been dirtied  since
> the file /proc/[pid]/clear_refs was written to.

The status of the `soft-dirty` bit of each page in the process can be checked
using the `/proc/pid/pagemap` file. This file contains a 64-bit integer for
every virtual page within the process, with bit `55` (the `soft-dirty` bit)
being set indicating that the Page Table Entry (*PTE*) has been modified:

> 63     If set, the page is present in RAM.
> 
> 62     If set, the page is in swap space
> 
> 61 (since Linux 3.5)
>        The page is a file-mapped page or a shared anonymous
>        page.
> 
> 60–57 (since Linux 3.11)
>        Zero
> 
> 56 (since Linux 4.2)
>        The page is exclusively mapped.
> 
> 55 (since Linux 3.11)
>        PTE is soft-dirty (see the kernel source file Documen‐
>        tation/admin-guide/mm/soft-dirty.rst).
> 
> 54–0   If the page is present in RAM (bit 63), then these bits
>        provide the page frame number, which can be used to
>        index /proc/kpageflags and /proc/kpagecount.  If the
>        page is present in swap (bit 62), then bits 4–0 give
>        the swap type, and bits 54–5 encode the swap offset.
> 
> Before Linux 3.11, bits 60–55 were used to encode the base-2
> log of the page size.
>
> To employ `/proc/pid/pagemap` efficiently, use
> `/proc/pid/maps` to determine which areas of memory are actu‐
> ally mapped and seek to skip over unmapped regions.

See the referenced kernel source file [Documentation/admin-guide/mm/soft-dirty.rst](https://github.com/torvalds/linux/blob/master/Documentation/admin-guide/mm/soft-dirty.rst)
for additional details.

#### Benchmarks of Snapshot Restoration Methods

I ended up experimenting with different snapshot restoration methods. These
were combinations of choosing the data ranges to restore and how to restore
them:

* Where
    * Restore full region data range at once
    * Restore dirty individual pages
* How
    * Direct seeks/writes to `/proc/pid/mem`
    * Using `process_vm_writev`

Below are two graphs from the benchmark. Click on either to view the by itself,
or view the [full benchmark results directly](snapshot_restore/report/index.html).

The benchmark includes the best- (dense) and worst-case (sparse) scenarios for
restoring pages in the target process by restoring entire memory regions at a
time. A `dense` scenario would mean that *every* page within a memory region
should be updated and counts towards the total count of pages restored. A
`sparse` scenario only counts one page per region towards the total count of
pages restored.

[![snapshot restore lines](snapshot_restore/report/lines.svg)](snapshot_restore/report/lines.svg)

Notice that the best-case scenarios (dense) for updating the entire region when
using both `/proc/pid/mem` and `process_vm_writev` are two of the best
performing snapshot restoration methods. These are unlikely to happen in
practice.

If we remove these benchmarks, we are left with this graph:

[![snapshot restore lines minus dense full region](snapshot_restore_no_dense.svg)](snapshot_restore_no_dense.svg)
