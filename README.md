# narly.me

This repository holds the content for my blog, [narly.me](https://narly.me)

## About

This site is built with [hugo](https://gohugo.io/)

## Building

First, ensure hugo is setup, and that you have pulled down the git submodules:

```
sudo snap install hugo
git init
git submodule update
```

Building the site is as straightforward as running `hugo` from the root of this
project.

Generated content is stored in the `public` directory.

## Adding New Posts

```bash
hugo new posts/the-new-post.md
```

Then edit the file at `content/posts/the-new-post.md`
